

FROM node:20-alpine

# Create app directory
WORKDIR /app

# Bundle files
COPY . .

#Update npm 
# RUN npm install -g npm@latest


# Install dependencies
RUN npm install
RUN npm i -g javascript-obfuscator
RUN npm run build
RUN rm -rf src
RUN npm run obfuscate
RUN rm -rf dist

# Expose port 3000
EXPOSE 3000 

# Start app
CMD source migrate-and-start.sh

# build docker image for github
# docker build . -t  ghcr.io/erada-fintech/services-nano:latest
#  push docker image to github
# docker push  ghcr.io/erada-fintech/services-nano:latest 

