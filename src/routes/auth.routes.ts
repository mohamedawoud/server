import { Router } from "express";

import { validateRequest } from "../middleware/index";
import { singIn__AuthController, singUp__AuthController, verifyOTP__AuthController } from "../controllers";
import { createUserSchema, loginSchema, verifySchema } from "../schema/index";
import { tryCatch } from "../utilities/index";

const router = Router();

router.post("/signin", validateRequest(loginSchema), tryCatch(singIn__AuthController));
router.post("/verify", validateRequest(verifySchema), tryCatch(verifyOTP__AuthController));
router.post("/signup", validateRequest(createUserSchema), tryCatch(singUp__AuthController));

export default router;
