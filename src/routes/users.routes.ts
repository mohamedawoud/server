import { updateUserController } from "./../controllers/users.controller";
import express from "express";
import { getUserController } from "../controllers";
import { validateRequest, validateUser } from "../middleware/index";
import { createUserSchema, loginSchema } from "../schema/index";
import { tryCatch } from "../utilities/index";
const router = express.Router();

router.get("/:id", tryCatch(validateUser), tryCatch(getUserController));
router.patch("/:id", tryCatch(validateUser), tryCatch(updateUserController));

// router.post(
//     "/",
//     // tryCatch(validateRole(["admin"])),
//     validateRequest(createUserSchema),
//     tryCatch(create_UsersController)
// );

// router.post("/session", validateRequest(loginSchema), tryCatch(getUserController));

// router.delete("/:username", tryCatch(validateUser), tryCatch(validateRole(["admin"])), tryCatch(deleteUserController));

export default router;
