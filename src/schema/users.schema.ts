import { object, string, number, ref } from "yup";

export const createUserSchema = object({
    body: object({
        firstName: string().required(),
        lastName: string().required(),
        password: string().required("password is required").min(8),
        confirmPassword: string()
            .required()
            .oneOf([ref("password"), null], "Passwords must match"),

        phoneNumber: string().matches(/^01[0125][0-9]{8}$/gm, "Invalid phone number"),
        email: string().email().required(),
        // authorityId: number().integer().positive(),
    })
        .strict(true)
        .noUnknown(true),
});
