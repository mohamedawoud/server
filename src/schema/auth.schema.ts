import { boolean, object, string } from "yup";

export const loginSchema = object({
    body: object({
        email: string().required("username is required"),
        password: string().required("password is required"),
    })
        .strict(true)
        .noUnknown(true),
});

export const verifySchema = object({
    body: object({
        email: string().required("email is required"),
        otp: string().required("password is required"),
        remember: boolean(),
    })
        .strict(true)
        .noUnknown(true),
});
