import bcrypt from "bcrypt";
import config from "../configs/config";

const salt = config.bcryptSalt;

export const encrypt = (password: string) => {
    return bcrypt.hashSync(password, salt);
};

export const compare = (inputPassword: string, hashedPassword: string) => {
    return bcrypt.compareSync(inputPassword, hashedPassword);
};
