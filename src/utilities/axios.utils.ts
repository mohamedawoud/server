import axios, { AxiosRequestConfig } from "axios";

export const axiosRequest = async (options: AxiosRequestConfig) => {
    try {
        const response = await axios.request(options);
        return response.data;
    } catch (error) {
        return error.response.data;
    }
};
