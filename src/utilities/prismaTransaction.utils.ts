import { prisma } from "../helpers";

export const handlePrismaTransaction = async (operations: any[]) => {
    return await prisma.$transaction(async (tx) => {
        return await Promise.all(
            operations.map((op) => {
                return typeof op === "function" && op(tx);
            })
        );
    });
};
