import config from "../configs/config";
import { logger } from "../utilities";

function ValidationErrorMapping(errorCode: number, message: string) {
    logger.info(JSON.stringify(errorCode));
    if (errorCode == 400) {
        return { errorCode: errorCode, message: message, ...config.OCR_ERRORS.BAD_REQUEST };
    } else if (errorCode === 415 || errorCode === 422) {
        return config.OCR_ERRORS.UNSUPPORTED_MEDIA;
    } else if (errorCode > 3000 && errorCode <= 3006) {
        return config.OCR_ERRORS.DOCUMENT_VALIDATION_ERROR;
    } else if (errorCode >= 3007 && errorCode <= 3009) {
        return config.OCR_ERRORS.INVALID_DATE;
    } else if (errorCode === 3010) {
        return config.OCR_ERRORS.DOCUMENT_EXPIRED;
    } else if (errorCode === 3011 || errorCode === 3013) {
        return config.OCR_ERRORS.CARD_NOT_DETECTED;
    } else if ((errorCode > 4000 && errorCode < 4211) || errorCode === 500) {
        return config.OCR_ERRORS.FACE_DETECTION_ERROR;
    } else if (errorCode > 5000 && errorCode < 5007) {
        return config.OCR_ERRORS.INVALID_ID;
    }
}

export default ValidationErrorMapping;
