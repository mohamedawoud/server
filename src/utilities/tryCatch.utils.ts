import { NextFunction, Request, Response } from "express";
import { ExtendedRequest } from "../types/extendedRequest";

const tryCatch = (controller: any) => async (req: ExtendedRequest | Request, res: Response, next: NextFunction) => {
    try {
        await controller(req, res, next);
    } catch (error) {
        return next(error);
    }
};
export default tryCatch;
