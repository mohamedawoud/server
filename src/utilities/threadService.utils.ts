import axios from "axios";
import { parentPort } from "worker_threads";

parentPort.on("message", async (data) => {
    const response = await axios.request(data);
    const responseData = response.data;
    parentPort.postMessage(responseData);
});
