import jwt, { JwtPayload } from "jsonwebtoken";
import config from "../configs/config";
import { logger } from "../utilities";

const verifyToken = async (token: string) => {
    try {
        const verify = jwt.verify(token, config.tokenSecret as string) as JwtPayload;
        return verify;
    } catch (error) {
        logger.error(JSON.stringify(error));
        return null;
    }
};

export default verifyToken;
