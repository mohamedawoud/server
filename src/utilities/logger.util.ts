import winston from "winston";
const { combine, timestamp, printf, colorize, align, uncolorize } = winston.format;
const now = new Date();
const year = now.getFullYear();
const month = now.getMonth() + 1; // Months are zero-indexed, so add 1
const day = now.getDate();

const options: winston.LoggerOptions = {
    transports: [
        new winston.transports.Console({
            level: process.env.NODE_ENV === "production" ? "error" : "debug",
        }),
        new winston.transports.File({
            filename: `logs/${year}/${month}/${day}/logs.log`,
            level: "debug",
            format: uncolorize(),
        }),
    ],
    format: combine(
        colorize({ all: true }),
        timestamp({ format: "YYYY-MM-DD hh:mm A" }),
        align(),
        printf((info: any) => `[${info.timestamp}] ${info.level}: ${info.message}`)
    ),
};

const logger = winston.createLogger(options);

if (process.env.NODE_ENV !== "production") logger.debug("Logging initialized at debug level");

export default logger;
