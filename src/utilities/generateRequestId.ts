export const generateRequestId = () => {
    // Generate a random 6-character alphanumeric string
    const characters = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
    let requestId = "";
    for (let i = 0; i < 6; i++) {
        requestId += characters.charAt(Math.floor(Math.random() * characters.length));
    }
    return requestId;
};
