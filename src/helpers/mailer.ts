/* eslint-disable @typescript-eslint/consistent-type-definitions */
import nodemailer from "nodemailer";
import path from "path";
import { logger } from "../utilities";
import config from "../configs/config";

interface IMailer {
    sendOtpMail(email: string, otp: string): void;
}

export default class Mailer implements IMailer {
    private static instance: Mailer;
    private static transporter: nodemailer.Transporter;

    private constructor() {}

    public static getInstance(): Mailer {
        if (!Mailer.instance) {
            this.transporter = nodemailer.createTransport({
                host: config.MAILER.HOST,
                port: Number(config.MAILER.PORT),
                auth: {
                    user: config.MAILER.USER,
                    pass: config.MAILER.PASS,
                },
            });

            Mailer.instance = new Mailer();
        }

        return Mailer.instance;
    }

    public async sendOtpMail(email: string, otp: string) {
        // get company image path from directory and add it to the html
        const imgPath = path.join(__dirname, "..", "..", "images", "erada_logo.png");

        logger.info(`Sending OTP from ${config.MAILER.USER} to ${email}`);

        const mailOptions = {
            from: config.MAILER.USER,
            to: email,
            subject: "Teste Templete ✔",
            html: HTMLRender(otp),
        };

        await Mailer.transporter.sendMail(mailOptions);

        console.log("Message sent: %s");
    }
}

// Mailer.transporter.sendMail(mailOptions, (error, info) => {
//     if (error) {n
//         return console.log(error);
//     }
//     console.log("Message %s sent: %s", info.messageId, info.response);
// });

//         const message = {
//             from: ` "QQ" ${config.MAILER.USER}`,
//             to: email,
//             subject: "QQ OTP",
//             text: `Please use the verification code below to sign in. ${otp}`,
//             html: `
//             <!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
//             <html xmlns="http://www.w3.org/1999/xhtml">
//               <head>
//                 <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
//                 <meta name="viewport" content="width=device-width, initial-scale=1.0" />
//                 <title>Verify your login</title>
//                 <!--[if mso
//                   ]><style type="text/css">
//                     body,
//                     table,
//                     td,
//                     a {
//                       font-family: Arial, Helvetica, sans-serif !important;
//                     }
//                   </style><!
//                 [endif]-->
//               </head>

//               <body style="font-family: Helvetica, Arial, sans-serif; margin: 0px; padding: 0px; background-color: rgb(239, 239, 239)">
//                 <table
//                   role="presentation"
//                   style="
//                     width: 100%;
//                     border-collapse: collapse;
//                     border: 0px;
//                     border-spacing: 0px;
//                     font-family: Arial, Helvetica, sans-serif;
//                     background-color: rgb(239, 239, 239);
//                   "
//                 >
//                   <tbody>
//                     <tr>
//                       <td align="center" style="padding: 1rem 2rem; vertical-align: top; width: 100%">
//                         <table
//                           role="presentation"
//                           style="
//                             max-width: 600px;
//                             border-collapse: collapse;
//                             border: 0px;
//                             border-spacing: 0px;
//                             text-align: center;
//                           "
//                         >
//                           <tbody>
//                             <tr>
//                               <td>
//                                 <div>
//                                   <div>
//                                     <img
//                                       src="erada_logo.png"
//                                       alt="Company"
//                                       style="height: 150px; margin-left: -66%"
//                                     />
//                                   </div>
//                                 </div>
//                                 <div style="padding: 20px; background-color: rgb(255, 255, 255)">
//                                   <div style="color: rgb(0, 0, 0); text-align: center">
//                                     <h1 style="margin: 1rem 0">Verification code</h1>
//                                     <p style="padding-bottom: 16px">
//                                       Please use the verification code below to sign in.
//                                     </p>
//                                     <p style="padding-bottom: 16px">
//                                       <strong style="font-size: 130%; letter-spacing: 12px">${otp}</strong>
//                                     </p>
//                                     <p style="padding-bottom: 16px">
//                                       If you didn’t request this, you can ignore this email.
//                                     </p>
//                                     <p style="padding-bottom: 16px">Thanks,<br />Erada Tech team</p>
//                                   </div>
//                                 </div>
//                               </td>
//                             </tr>
//                           </tbody>
//                         </table>
//                       </td>
//                     </tr>
//                   </tbody>
//                 </table>
//               </body>
//             </html>

// `,
//         };

const HTMLRender = (otp: any) =>
    `
            <!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
            <html xmlns="http://www.w3.org/1999/xhtml">
              <head>
                <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
                <meta name="viewport" content="width=device-width, initial-scale=1.0" />
                <title>Verify your login</title>
                <!--[if mso
                  ]><style type="text/css">
                    body,
                    table,
                    td,
                    a {
                      font-family: Arial, Helvetica, sans-serif !important;
                    }
                  </style><!
                [endif]-->
              </head>
            
              <body style="font-family: Helvetica, Arial, sans-serif; margin: 0px; padding: 0px; background-color: rgb(239, 239, 239)">
                <table
                  role="presentation"
                  style="
                    width: 100%;
                    border-collapse: collapse;
                    border: 0px;
                    border-spacing: 0px;
                    font-family: Arial, Helvetica, sans-serif;
                    background-color: rgb(239, 239, 239);
                  "
                >
                  <tbody>
                    <tr>
                      <td align="center" style="padding: 1rem 2rem; vertical-align: top; width: 100%">
                        <table
                          role="presentation"
                          style="
                            max-width: 600px;
                            border-collapse: collapse;
                            border: 0px;
                            border-spacing: 0px;
                            text-align: center;
                          "
                        >
                          <tbody>
                            <tr>
                              <td>
                                <div>
                                  <div>
                                    <img
                                      src="erada_logo.png"
                                      alt="Company"
                                      style="height: 150px; margin-left: -66%"
                                    />
                                  </div>
                                </div>
                                <div style="padding: 20px; background-color: rgb(255, 255, 255)">
                                  <div style="color: rgb(0, 0, 0); text-align: center">
                                    <h1 style="margin: 1rem 0">Verification code</h1>
                                    <p style="padding-bottom: 16px">
                                      Please use the verification code below to sign in.
                                    </p>
                                    <p style="padding-bottom: 16px">
                                      <strong style="font-size: 130%; letter-spacing: 12px">${otp}</strong>
                                    </p>
                                    <p style="padding-bottom: 16px">
                                      If you didn’t request this, you can ignore this email.
                                    </p>
                                    <p style="padding-bottom: 16px">Thanks,<br />Erada Tech team</p>
                                  </div>
                                </div>
                              </td>
                            </tr>
                          </tbody>
                        </table>
                      </td>
                    </tr>
                  </tbody>
                </table>
              </body>
            </html>
            
`;
