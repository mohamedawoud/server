import { PrismaClient } from "@prisma/client";
import { readReplicas } from "@prisma/extension-read-replicas";
// import { logEvents } from "../middleware";
// import { readReplicas } from "@prisma/extension-read-replicas";

const prisma = new PrismaClient();
// .$extends(
//     readReplicas({
//         url: [
//             "postgresql://postgres:123456789@127.0.0.1:5433/nano",
//             // "postgresql://postgres:123456789@127.0.0.1:5432/nano_replica12",
//         ],
//     })
// );
// if (process.env.NODE_ENV === "production") {
//     prisma.$on("query", (e: any) => {
//         logEvents.info("Query: " + e.query);
//         logEvents.info("Params: " + e.params);
//         logEvents.info("Duration: " + e.duration + "ms");
//         logEvents.info("Timestamp: " + e.timestamp);
//     });
// }
export default prisma;
