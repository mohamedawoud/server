import { fuzzy, Searcher } from "fast-fuzzy";

export function searchWord(word: string, originalWord: string) {
    const rate = fuzzy(word, originalWord);
    if (rate > 0.7) return true;

    return false;
}

export function fuzzySearchArray(word: string, array: []) {
    const searcher = new Searcher(array);
    const matching = searcher.search(word);

    if (matching.length > 0) return true;
    return false;
}
