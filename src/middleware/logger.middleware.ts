import { NextFunction, Request, Response } from "express";
import { logger } from "../utilities";

export const logMiddleware = (req: Request, res: Response, next: NextFunction) => {
    logger.info(`${req.method}  ${req.url}  ${req.headers["user-agent"]}  ${req.ip}`);
    next();
};
