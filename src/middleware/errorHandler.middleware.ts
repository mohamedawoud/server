import { NextFunction, Response } from "express";
import { ExtendedRequest } from "../types/extendedRequest";
import { logger } from "../utilities";

const errorHandler = (error: any, req: ExtendedRequest, res: Response, next: NextFunction) => {
    logger.error(`[${error.errorStatusCode || 500}] ${error.name}:  ${error.message}`);
    logger.error(`${JSON.stringify(error)}`);
    if (error.stack) console.log(error.stack);
    return res.status(error.errorStatusCode || 500).send({ message: error.message });
};

export default errorHandler;
