import { NextFunction, Response } from "express";
import { logger } from "../utilities";
import { Schema } from "yup";
import { ExtendedRequest } from "../types/extendedRequest";

export const validateRequest = (schema: Schema) => async (req: ExtendedRequest, res: Response, next: NextFunction) => {
    try {
        await schema.validate({
            body: req.body,
            query: req.query,
            params: req.params,
        });
        return next();
    } catch (error) {
        logger.error(`${error.name}:  ${error.message}`);
        return res.status(400).send({ name: error.name, message: error.message });
    }
};
