import { NextFunction, Response } from "express";
import { verifyToken } from "../utilities";
import { ExtendedRequest } from "../types/extendedRequest";

export const validateUser = async (req: ExtendedRequest, res: Response, next: NextFunction) => {
    if (!req.headers.authorization) return res.sendStatus(403);
    const accessToken = req.headers.authorization.split(" ")[1];
    const verified = await verifyToken(accessToken);
    console.log("🚀 ~ validateUser ~ verified:", verified);
    if (!verified) return res.status(403).send("authorization required");
    req.user = verified;
    return next();
};
