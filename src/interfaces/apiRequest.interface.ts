/* eslint-disable no-unused-vars */
import { AxiosRequestConfig, AxiosResponse } from "axios";

export interface ApiRequestStrategy {
    request(options: AxiosRequestConfig): Promise<AxiosResponse | any>;
}
