import express, { Request, Response, json, urlencoded } from "express";
import cookieParser from "cookie-parser";
import timeout from "connect-timeout";
import cors from "cors";

import { AuthRoutes, UserRoutes } from "./routes/index";
import { errorHandler, logMiddleware } from "./middleware/index";
import { logger } from "./utilities/index";
import config, { CROS } from "./configs/config";
import { generateRequestId } from "./utilities/generateRequestId";

const app = express();
const port = config.PORT;

// Configerations
app.use(timeout("50s"));
app.use(json({ limit: "10mb" }));
app.use(urlencoded({ limit: "10mb", extended: true }));
app.use(cors(CROS));
app.use(cookieParser());

// Logical Logger
app.use(logMiddleware);

// Authantication
app.use("/", AuthRoutes);

// Resources
app.use("/users", UserRoutes);

// Fallback
app.all("*", fallback);

// Error Handling
app.use(errorHandler);

// Bootstrap
app.listen(port, async () => {
    logger.info(
        `The server process no ${process.pid} is running in the ${process.env.NODE_ENV || "development"} mode listing on port ${port} `
    );
});

function fallback(req: Request, res: Response) {
    logger.error(`[404] NotFound:   Can't find the resourse ${req.url} on the server`);
    res.status(404).send({ message: "Can't find the resourse on the server" });
}
