/* eslint-disable no-undef */
import "dotenv/config";

export const CROS = {
    origin: "http://localhost:4200",
};

const errorMessages = {
    customerNotFound: "Customer not found",
    invalidRequestedAmount: "requestedAmount cannot be more than the credit limit",
    nIdNotVerified: "Customer national id images must be verified",
    inaccurateLoanDuration: "loanDuration and repaymentCycle are not Accurate",
    oneLoanAllowed: "only one loan should be open",
    loanNotFound: "loan not found ",
    nIdValidationError: "Customer Created but the national id provided is not valid",
    ocrValidationError: "Validation Error,Please make sure the document is in a good lighting condition and well focused.",
    ocrFormat: "Make sure dates in the ID are in correct format",
    documentExpired: "Document Expired",
    serverError: "Something Went Wrong",
    invalidId: "Please Provide A Valid Id",
    lowCreditLimit: "credit limit is too low.",
    idsNotMatch: "Extracted NID doesn't match the provided NID",
};

const config = {
    NODE_ENV: process.env.NODE_ENV || "development",
    PORT: process.env.PORT || 3000,
    bcryptSalt: process.env.SALT || 12,
    tokenSecret: process.env.TOKEN_SECRET,
    tokenExpireTime: process.env.TOKEN_EXPIRE_TIME || "24h",
    tokenExpireTimeRemember: process.env.TOKEN_EXPIRE_TIME_REMEMBER || "7d",

    MAILER: {
        // host: "smtp.ethereal.email",
        HOST: process.env.MAILER_HOST || "",
        PORT: process.env.MAILER_PORT || 587,
        // user: "macie.schuster@ethereal.email",
        USER: process.env.MAILER_USER || "",
        // pass: "jpvrPBYp9rUKpVKZjk",
        PASS: process.env.MAILER_PASS || "",
    },

    AUTHENTICATION_ERRORS: {
        USER_EXISTS: { errorStatusCode: 409, message: "user already exist", errorCode: "US_1" },
        INVALID_USER: { errorStatusCode: 401, message: "invalid username or password", errorCode: "US_2" },
        WRONG_PASSWORD: { errorStatusCode: 403, message: "invalid username or password", errorCode: "US_3" },
        WRONG_OTP: { errorStatusCode: 400, message: "invalid OTP", errorCode: "US_4" },
    },

    vLensUrl: process.env.VLENS_URL,
    vLensApiKey: process.env.VLENS_API_KEY,
    vLensBundleKey: process.env.VLENS_BUNDLE_KEY,
    iScoreUrl: process.env.ISCORE_URL,
    iScoreApiKey: process.env.ISCORE_API_KEY,
    iScorePassword: process.env.ISCORE_PASSWORD,
    iScoreUserId: process.env.ISCORE_USER_ID,
    penaltyDuration: 1,
    penaltyDurationType: "daily",
    earlySettlementRevenue_COA: { number: "13300012", name: "Early Settlement Revenue" },
    WriteOffLoan_COA: { number: "13300014", name: "Write Off principle" },
    advencedPayment_COA: { number: "12200010", name: "Advanced" },
    principle_COA: { number: "12200001", name: "principle" },
    cashWallet_COA: { number: "122033304", name: "cashWallet" },
    latePenalty_COA: { number: "123123123", name: "latePenalty" },
    interestRate_COA: { number: "12321213", name: "interestRate" },
    accountPayableEtisalat_COA: { number: "1232221213", name: "accountPayableEtisalat" },
    accountReceivable_COA: { number: "12229302", name: "accountReceivable" },
    accountWriteOffPrincple_COA: { number: "122234354", name: "WriteOffPrincple" },
    glDebitDirection: "DR",
    glCreditDirection: "CR",
    etisalatPayablePercentage: 0.05,
    LOAN_ERRORS: {
        CUSTOMER_NOT_FOUND: { errorStatusCode: 404, message: errorMessages.customerNotFound, errorCode: "LN_1" },
        INVALID_REQUESTED_AMOUNT: {
            errorStatusCode: 400,
            message: errorMessages.invalidRequestedAmount,
            errorCode: "LN_2",
        },
        NID_NOT_VERIFIED: {
            errorStatusCode: 400,
            message: errorMessages.nIdNotVerified,
            errorCode: "LN_3",
        },
        INACCURATE_LOAN_DURATION: {
            errorStatusCode: 400,
            message: errorMessages.inaccurateLoanDuration,
            errorCode: "LN_4",
        },
        ONE_LOAN_ALLOWED: { errorStatusCode: 400, message: errorMessages.oneLoanAllowed, errorCode: "LN_5" },
        UPDATE_LOAN_CUSTOMER_NOT_FOUND: {
            errorStatusCode: 404,
            message: errorMessages.customerNotFound,
            errorCode: "LN_6",
        },
        LOAN_NOT_FOUND: { message: errorMessages.loanNotFound, errorStatusCode: 404, errorCode: "LN_7" },
        GET_LOANS_NOT_FOUND: { errorStatusCode: 404, message: errorMessages.loanNotFound, errorCode: "LN_8" },
        LOW_CREDIT_LIMIT: { errorStatusCode: 400, message: errorMessages.lowCreditLimit, errorCode: "LN_9" },
    },
    CUSTOMER_ERRORS: {
        CUSTOMER_NOT_FOUND: {
            message: errorMessages.customerNotFound,
            errorStatusCode: 404,
            errorCode: "CS_1",
        },
        NATIONAL_ID_NOT_VALID: {
            message: errorMessages.nIdValidationError,
            errorCode: "CS_2",
            errorStatusCode: 400,
        },
        PATCH_CUSTOMER_NOT_FOUND: {
            message: errorMessages.customerNotFound,
            errorStatusCode: 404,
            errorCode: "CS_3",
        },
        PATCH_NID_NOT_VALID: {
            errorStatusCode: 400,
            errorCode: "CS_4",
        },
    },
    OCR_ERRORS: {
        BAD_REQUEST: {
            errorCode: "OCR_1",
        },
        UNSUPPORTED_MEDIA: { errorStatusCode: 415, message: "Unsupported Media Type", errorCode: "OCR_2" },
        DOCUMENT_VALIDATION_ERROR: {
            errorStatusCode: 400,
            message: errorMessages.ocrValidationError,
            errorCode: "OCR_3",
        },
        INVALID_DATE: {
            errorStatusCode: 400,
            message: errorMessages.ocrFormat,
            errorCode: "OCR_4",
        },
        DOCUMENT_EXPIRED: {
            errorStatusCode: 400,
            message: errorMessages.documentExpired,
            errorCode: "OCR_5",
        },
        CARD_NOT_DETECTED: {
            errorStatusCode: 400,
            messge: errorMessages.ocrValidationError,
            errorCode: "OCR_6",
        },
        FACE_DETECTION_ERROR: { errorStatusCode: 500, message: errorMessages.serverError, errorCode: "OCR_7" },
        INVALID_ID: { errorStatusCode: 406, message: errorMessages.invalidId, errorCode: "OCR_8" },
        NIDS_NOT_MATCH: {
            errorStatusCode: 400,
            message: errorMessages.idsNotMatch,
            errorCode: "OCR_9",
        },
    },

    SCORE_WEIGHTS: {
        AGE_WEIGHT: 20,
        GENDER_WEIGHT: 20,
        EDUCATION_WEIGHT: 30,
        MARITAL_STATUS_WEIGHT: 30,
    },
    MAX_SCORE: 5,
    MONTHLY_INTEREST_RATE: 10,
    SAMPLE_VLENS_RES1: {
        services: {
            Validations: {
                validation_errors: [] as [],
            },
            spoofing: {
                fake: false,
            },
            classification: {
                doc_type: "id_front",
            },
            liveness: null as null,
            AML: {
                AML_matched: false,
                data: [] as [],
            },
        },
        data: {
            name: "عمر قاصد خلف الدكرونى",
            address: "المنشاه مركز المنشاه - سوهاج",
            first_name: "عمر",
            last_names: "قاصد خلف الدكرونى",
            govern: "سوهاج",
            idKey: "IZ7772178",
            dateOfBirth: "1993-06-30T00:00:00",
            address_1: "المنشاه",
            address_2: "مركز المنشاه - سوهاج",
            idNumber: "29306302600792",
            gender: null as null,
            client_transaction_id: null as null,
            request_id: "00000000-0000-0000-0000-000000000000",
            transaction_id: "4b0d94e8-df36-4b30-a7c2-b0ae62b24950",
        },
        error_code: null as null,
        error_message: null as null,
    },
    SAMPLE_VLENS_RES2: {
        services: {
            Validations: {
                validation_errors: [] as [],
            },
            spoofing: null as null,
            classification: {
                doc_type: "id_back",
            },
            liveness: null as null,
            AML: null as null,
        },
        data: {
            maritalStatus: "متزوج",
            job: "حاصل على دبلوم صنايع",
            jobTitle: "",
            religion: "مسلم",
            husbandName: "",
            releaseDate: "2021-08-01T00:00:00",
            idExpiry: "2028-08-15T00:00:00",
            idNumber: "29306302600792",
            gender: "ذكر",
            client_transaction_id: null as null,
            request_id: "00000000-0000-0000-0000-000000000000",
            transaction_id: "b84faf3e-4b5f-4e1c-b120-ef3e2b5ea914",
        },
        error_code: null as null,
        error_message: null as null,
    },
    EARLY_SETTLEMENT_COMMISION: 0.05,
    DISBURSEMENTPOOL: 10_000_000,
};

export default config;
