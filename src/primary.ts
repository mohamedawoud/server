import cluster from "cluster";
import { cpus } from "os";
import { resolve } from "path";
import { logger } from "./utilities";

const numCPUs = cpus().length;
logger.info(`Number of CPUs: ${numCPUs}`);
logger.info(`Master no. ${process.pid} is started`);

cluster.setupPrimary({
    exec: resolve(__dirname, "index"),
});

for (let i = 0; i < numCPUs; i++) {
    cluster.fork();
}

cluster.on("exit", (worker, code) => {
    logger.error(`worker no. ${worker.process.pid} died`);
    logger.error(`code: ${code}`);
    logger.debug("starting a new worker");
    cluster.fork();
});
