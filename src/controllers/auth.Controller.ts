import jwt from "jsonwebtoken";
import otpGenerator from "otp-generator";
import { Response } from "express";

import config from "../configs/config";

import { createUser, getUser, updateUser } from "../services";
import { Mailer } from "../helpers";
import { compare, encrypt, logger } from "../utilities";
import { ExtendedRequest } from "../types/extendedRequest";

export const singUp__AuthController = async (req: ExtendedRequest, res: Response) => {
    const { email, password, firstName, lastName, phoneNumber } = req.body;

    const hashedPassword = encrypt(password);

    const USER = await createUser({
        password: hashedPassword,
        email: email as string,
        firstName: firstName as string,
        lastName: lastName as string,
        phoneNumber: Number(phoneNumber),
    });
    if (!USER) throw config.AUTHENTICATION_ERRORS.USER_EXISTS;

    return res.status(201).send({ message: "User created successfully", data: USER });
};

export const singIn__AuthController = async (req: ExtendedRequest, res: Response) => {
    const { email, password, remember } = req.body;

    let USER = await getUser({ email });
    if (!USER) throw config.AUTHENTICATION_ERRORS.INVALID_USER;

    const isPasswordMatch = compare(password, USER.password);
    if (!isPasswordMatch) throw config.AUTHENTICATION_ERRORS.WRONG_PASSWORD;

    const OTP = otpGenerator.generate(6, { upperCaseAlphabets: false, specialChars: false, lowerCaseAlphabets: false });

    const hashedOTP = encrypt(OTP);
    USER = await updateUser({ userId: USER.userId }, { otp: hashedOTP });

    // const mailer = Mailer.getInstance();
    // await mailer.sendOtpMail(USER.email, OTP);

    console.log("🚀 ~ constsingIn__AuthController= ~ OTP:", OTP);
    res.status(200).send({ message: "OTP Successful Sent", OTP });
};

export const verifyOTP__AuthController = async (req: ExtendedRequest, res: Response) => {
    const { otp, email, remember } = req.body;

    const USER = await getUser({ email: email });
    if (!USER) throw config.AUTHENTICATION_ERRORS.INVALID_USER;

    console.log("🚀 ~ constsingIn__AuthController= ~ OTP:", USER);

    const isOTPMatch = compare(otp, USER.otp ? USER.otp : "");
    if (!isOTPMatch) throw config.AUTHENTICATION_ERRORS.WRONG_OTP;

    const signature = jwt.sign({ email: USER.email }, config.tokenSecret, {
        expiresIn: remember ? config.tokenExpireTimeRemember : config.tokenExpireTime,
    });

    logger.info(`User has signIn:  ${USER.email}`);
    res.status(200).json({
        message: "OTP verified successfully",
        data: {
            routeId: USER.uuid,
            token: signature,
        },
    });

    await updateUser({ userId: USER.userId }, { otp: "" });
};
