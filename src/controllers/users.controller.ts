import config from "../configs/config";
import { getUser, updateUser } from "../services";
import { ExtendedRequest } from "../types/extendedRequest";
import { Response } from "express";

export const getUserController = async (req: ExtendedRequest, res: Response) => {
    const { email } = req.user;
    const user = await getUser({ email });
    console.log("🚀 ~ getUserController ~ user:", user);
    if (!user) throw config.AUTHENTICATION_ERRORS.INVALID_USER;

    return res.status(200).send({
        message: "User fetched successfully",
        data: {
            routeId: user.uuid,
            email: user.email,
            firstName: user.firstName,
            lastName: user.lastName,
            phoneNumber: user.phoneNumber,
        },
    });
};

export const updateUserController = async (req: ExtendedRequest, res: Response) => {
    const { email } = req.user;

    const isUserExist = await getUser({ email });
    if (!isUserExist) throw config.AUTHENTICATION_ERRORS.INVALID_USER;

    const user = await updateUser(
        { email },
        {
            email,
            firstName: req.body.firstName,
            lastName: req.body.lastName,
            phoneNumber: req.body.phoneNumber,
        }
    );

    if (!user) throw config.AUTHENTICATION_ERRORS.USER_EXISTS;
    return res.status(201).send(user);
};
