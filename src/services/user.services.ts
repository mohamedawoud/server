import { Prisma } from "@prisma/client";
import { prisma, prismaErrorMapping } from "../helpers";
import { logger } from "../utilities";

export const createUser = async (query: Prisma.UserCreateInput) => {
    try {
        const user = await prisma.user.create({
            data: query,
            select: {
                uuid: true,
                email: true,
                createdAt: true,
            },
        });
        return user;
    } catch (error) {
        logger.error(`${error.name}:  ${error.message}`);
        throw prismaErrorMapping(error);
    }
};

export const updateUser = async (where: Prisma.UserWhereUniqueInput, data: Prisma.UserUpdateInput) => {
    try {
        const user = await prisma.user.update({ where: where, data: data });
        return user;
    } catch (error) {
        logger.error(`${error.name}:  ${error.message}`);
        throw prismaErrorMapping(error);
    }
};

///****** */

export const getUser = async (query: Prisma.UserWhereUniqueInput) => {
    try {
        const user = await prisma.user.findUnique({ where: query });
        return user;
    } catch (error) {
        logger.error(`${error.name}:  ${error.message}`);
        throw prismaErrorMapping(error);
    }
};

export const getUsers = async () => {
    try {
        const users = await prisma.user.findMany();
        return users;
    } catch (error) {
        logger.error(`${error.name}:  ${error.message}`);
        throw prismaErrorMapping(error);
    }
};

export const deleteUser = async (where: Prisma.UserWhereUniqueInput) => {
    try {
        const user = await prisma.user.deleteMany({ where: where });
        return user;
    } catch (error) {
        logger.error(`${error.name}:  ${error.message}`);
        throw prismaErrorMapping(error);
    }
};
